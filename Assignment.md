# Outline

## TCP Hands-on
1. Simple TCP server, test with telnet, nc (no send/recv, closes conn on accepting client conn, prints client details before closing)
2. Simple TCP client, connects to server and closes connection
3. Extend server to perform send, recv operations, test with telnet/nc
4. Client - Server communication
   a. server sends one msg on accept, client will acknowledge
   b. client will send one msg on connect, server will acknowledge (echo server)
   c. multiple messages between client & server, client keep reading messages from
      STDIN and send to server, server will reply some,
      client closes connection if input string is "quit"
      server closes connection is received string is "stop"

## TCP Assignment
5. File transfer between client & server
   * upload - client will read src file and send to server over socket, server will read 
     from socket(client) and store in dest file.
   * download - server will keep reading file and send to client via socket, client
     will receive data and store in the file.
6. Asynchronous communication between client & server (like IM chat)
   * Create two threads each in client & server, one for sending other for receiving
   * Sender thread, reads messages from STDIN and writes to socket
   * Receiver thread, reads messages from socket and print on STDOUT
   * Sender thread will close write channel (hint:- SHUTDOWN) if input string is "Bye",
     and thread will terminate
   * Receiver thread will close read channel, when other end has closed write channel (Hint:-
     zero return value) and thread will terminate
   * main thread will join for both the threads and close sockets
7. Asynchronous file transfer, upload + download simultaneously using threads
8. Concurrent server using fork
9. Concurrent server using threads

## UDP Hands-on
* Simple UDP sender & receiver, e.g. echo server

## UDP Assignment
* Retrieve current date & time of server
* Retrieve file attributes from server (of specified file), hint:- lstat


